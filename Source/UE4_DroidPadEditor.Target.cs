

using UnrealBuildTool;
using System.Collections.Generic;

public class UE4_DroidPadEditorTarget : TargetRules
{
	public UE4_DroidPadEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "UE4_DroidPad" } );
	}
}
