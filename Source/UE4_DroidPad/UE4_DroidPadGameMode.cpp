

#include "UE4_DroidPad.h"
#include "UE4_DroidPadGameMode.h"
#include "UE4_DroidPadPlayerController.h"

AUE4_DroidPadGameMode::AUE4_DroidPadGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = AUE4_DroidPadPlayerController::StaticClass();
}


